var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');


//console.log(util.responseconfig.SUCCESS);

//::LIST
router.get('/', function (req, res) {

    var params = req.body;
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 100);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status,1);
    var cityid = util.IsNull(req.query.cityid);

    var sql = "SELECT cc.*,c.city_name FROM collectioncenter as cc left join citylist as c ON    (cc.centeraddress::json->>'city')::integer = c.city_id where true ";
    if (status != null && status != "") {
        sql += " AND cc.centerstatus= $3 "; 
    }
    if(cityid != null){
        sql += " AND (cc.centeraddress::json->>'city')::integer = $4 "; 
    }

    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }
    sql += " offset $1 limit $2 ";
    console.log('collection list ', sql, offset, limit);
    db.any(sql, [offset, limit,status,cityid])
        .then(function (results) {
            res.status(200);
            res.send(
                /* {
                message: "users",
                id: results[0].currval
            } */
                results
            );
        })
        .catch(function (err) {
            console.log('collection center list',err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});


//::GETITEM
router.get('/:id', function (req, res) {
    var params = req.body;
    var id = req.params.id

    var sql = "SELECT * FROM collectioncenter"
    sql += " WHERE centerid=$1"
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

//::GET ALL COLLETION WITH OUT LIMIT TO DISPLAY ALL IN DROPBOX LIST 
router.get('/collectionlist/city', function (req, res) {
  
    var cityid = util.IsNull(req.query.cityid, 0);

    var sql = "SELECT * FROM collectioncenter"
    sql += " WHERE (centeraddress::json->>'city') = $1  AND centerstatus=1";
    db.any(sql, [cityid])
        .then(function (results) {
           // var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            // error;
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});
//::ADD
router.post('/', function (req, res) {
    var params = req.body;

    var centername =  util.IsNull(params.centername);
    var centertype = util.IsNull(params.centertype);
    var contactno = util.IsNull(params.contactno);
    var contactperson = util.IsNull(params.contactperson);
    var centeraddress = util.IsNull(params.centeraddress);
    var centeremail = util.IsNull(params.centeremail); 
    var centerstatus = 1;
    var registrationno = util.IsNull(params.registrationno);
    var postalcode = util.IsNull(params.postalcode);
    var addedby = util.IsNull(params.addedby);   


        var sql = "SELECT * FROM collectioncenter WHERE centername=$1";
       db.any(sql, [centername])
        .then(function (results) {

            if (results != null && results.length == 0) {

                var sql = "INSERT INTO collectioncenter(centername ,centertype ,contactno ,contactperson ,centeraddress ,centeremail ,centerstatus,registrationno,postalcode,addedby)"
                sql += " VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)";
            
                db.any(sql, [centername ,centertype ,contactno ,contactperson ,centeraddress ,centeremail ,centerstatus ,registrationno,postalcode,addedby
                    ])
                    .then(function (results) {
                        res.status(201);
                        res.send({
                            status:201,
                            message: "Collectioncenter created"               
                        });
                    })
                    .catch(function (err) {
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });

            } else {
                res.status(200);
                res.send({
                    status:202,
                    message: "Collection Center name is already exist"                      
                });
            }
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });  

});

//::UPDATE
router.post('/:id', function (req, res) {
    var params = req.body;

    var id = req.params.id
    var centername = util.IsNull(params.centername);
    var centertype = util.IsNull(params.centertype);
    var contactno = util.IsNull(params.contactno);
    var contactperson = util.IsNull(params.contactperson);
    var centeraddress = util.IsNull(params.centeraddress);
    var centeremail = util.IsNull(params.centeremail); 
    var centerstatus = util.IsNull(params.centerstatus);
    var registrationno = util.IsNull(params.registrationno);
    var postalcode = util.IsNull(params.postalcode);
    var updatedby = util.IsNull(params.updatedby);


    var sql = "UPDATE collectioncenter SET centername =coalesce($2,centername),centertype =coalesce($3,centertype),contactno=coalesce($4,contactno),";
    sql += "contactperson=coalesce($5,contactperson),centeraddress=coalesce($6,centeraddress),centeremail=coalesce($7,centeremail),";
    sql += "centerstatus=coalesce($8,centerstatus),registrationno=coalesce($9,registrationno),";
    sql += "postalcode=coalesce($10,postalcode),updatedby=coalesce($11,updatedby)";
    sql += " WHERE centerid=$1";
   
    db.any(sql, [id, centername ,centertype ,contactno ,contactperson ,centeraddress ,centeremail ,centerstatus ,registrationno,postalcode, updatedby
        ])
        .then(function (results) {
            res.status(200);
            res.send({
                status:201,
                message: "Collectioncenter update",
                id: id
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});





 
module.exports = router;