var config = require('./../config/dbconfig');
var pg = require('pg');
const pgp = require('pg-promise')();
var client;
var db;

db = pgp(config.dbConnectionString);
client = new pg.Client(config.dbConnectionString);
client.connect();

client.query('LISTEN watchers');
client.on('notification', function (data) {
    console.log(data);
});
//****************db trigger for notification*************//  
module.exports = db;

