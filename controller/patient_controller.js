var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');
 

//::ADD
/*
mandatory fields
username , eamil, role
*/
router.post('/', function (req, res) {
    var params = req.body;

    var patientname = util.IsNull(params.patientname);
   
    var email = util.IsNull(params.email);
    var mobileno = util.IsNull(params.mobileno);
    var address = util.IsNull(params.address);
    var postalcode = util.IsNull(params.postalcode);
    var userrole = util.IsNull(params.userrole);   
    var userstatus = 1;
    var addedby = util.IsNull(params.addedby);
    var centerid = util.IsNull(params.centerid);
    var idproofno = util.IsNull(params.idproofno);

    var sqlCheckUsername = "SELECT * FROM patient WHERE mobileno=$1";
    db.any(sqlCheckUsername, [mobileno])
        .then(function (results) {

            if (results != null && results.length == 0) {
                var sql = "INSERT INTO patient(patientname, email, mobileno, address, postalcode, userrole, userstatus, addedon, addedby, centerid, idproofno) "                
                sql += " VALUES($1,$2,$3,$4,$5,$6,$7,now(),$8,$9,$10) RETURNING patientid"; 
                db.any(sql, [patientname, email, mobileno, address, postalcode, userrole,userstatus, addedby, centerid,idproofno
                    ])
                    .then(function (results) { 
                        res.status(200);
                        res.send({
                            status: 201,
                            message: "Patient created",
                            userid: results[0].patientid,
                            name:results[0].patientname
                        });
                    })
                    .catch(function (err) {
                        console.log('insert patient error',err);
                        var code = err.code == 23505 ? 409 : 400
                        res.status(code);
                        res.send(err);
                    });

            } else {
                res.status(200);
                res.send({
                    message: "Name is already exist",
                    userid: results[0].patientid,
                    name:results[0].patientname
                });
            }
        })
        .catch(function (err) {
            console.log('user insert error ::', err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});




//::LIST
router.get('/', function (req, res) { 

     
    var offset = util.IsNull(req.query.offset, 0);
    var limit = util.IsNull(req.query.limit, 10);
    var orderby = util.IsNull(req.query.orderby);
    var sortby = util.IsNull(req.query.sortby);
    var status = util.IsNull(req.query.status);
    var searchString = util.IsNull(req.query.searchString);
    var searchType = util.IsNull(req.query.searchType);
   

    var sql = "select patientid, email, mobileno, postalcode,comments,  patientname from patient where TRUE  ";

   

    if (searchString != null && searchType != null) {
        sql += " AND " + searchType + " ILIKE '%" + searchString + "%' ";
    }
   
    if (orderby != null && orderby != "") {
        sql += " order by " + orderby;
        if (sortby != null && sortby != "") {
            sql += " " + sortby;
        }
    }
    sql += " offset $1 limit $2 ";
    console.log('patientlist', sql);
    db.any(sql, [offset, limit])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});


router.get('/:id', function (req, res) {
    
    var id = req.params.id

    var sql = "SELECT * FROM patient WHERE patientid=$1"
    db.any(sql, [id])
        .then(function (results) {
            var result = (results.length > 0) ? results[0] : {}
            res.status(200);
            res.send(result);
        })
        .catch(function (err) { 
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });
});

 

 

  




 

module.exports = router;