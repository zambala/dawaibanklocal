var express = require('express');
var router = express.Router();
var db = require('./dbconnections_controller');
var util = require('./../config/helper');



//::LIST
router.get('/citylistbycountry', function (req, res) {

    console.log('citylistbycountry');

    var countryid = util.IsNull(req.query.countryid, 101);
    var sql = "SELECT id from statelist  where country_id IN($1) "; //AND status=TRUE "; 


    db.any(sql, [countryid])
        .then(function (results) {
            var stateIdList = results.map(function (stateid) {
                return stateid.id.toString();
            });

            var sqlcity = "SELECT  * from citylist  where state IN("+stateIdList +") and   status=TRUE ";
            console.log(sqlcity);
            db.any(sqlcity).then(function (resultscity) {
                res.status(200);
                res.send(resultscity);
            }).catch(function (err) {
                console.log('citylist error', err);
                var code = err.code == 23505 ? 409 : 400
                res.status(code);
                res.send(err);
            });
        })
        .catch(function (err) {
            console.log(err);
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});





//::LIST
router.get('/citylist', function (req, res) {

    
   /*
    var countryid = util.IsNull(req.query.countryid, 'IN');
    var sql = "SELECT  * from citylist  where status=TRUE ";
    if (countryid != null) {
        sql += " AND countryid =$1 ";
    }
    sql += "  order by city_name asc "; 
    db.any(sql, [countryid])
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

        */


       var countryid = util.IsNull(req.query.countryid, 101);
       var sql = "SELECT id from statelist  where country_id IN($1) AND status=TRUE "; 
       
   
       db.any(sql, [countryid])
           .then(function (results) {
               var stateIdList = results.map(function (stateid) {
                   return stateid.id.toString();
               });
               console.log(stateIdList);
               var sqlcity = "SELECT  * from citylist  where state IN("+stateIdList +") and status=TRUE order by city_name asc ";
               console.log(sqlcity);
               db.any(sqlcity).then(function (resultscity) {
                   res.status(200);
                   res.send(resultscity);
               }).catch(function (err) {
                   console.log('citylist error', err);
                   var code = err.code == 23505 ? 409 : 400
                   res.status(code);
                   res.send(err);
               });
           })
           .catch(function (err) {
               console.log(err);
               var code = err.code == 23505 ? 409 : 400
               res.status(code);
               res.send(err);
           });
   

});

//::LIST
router.get('/countrylist', function (req, res) {

    var sql = "SELECT  * from countrylist  where status=TRUE order by countryname asc ";
    db.any(sql)
        .then(function (results) {
            res.status(200);
            res.send(results);
        })
        .catch(function (err) {
            var code = err.code == 23505 ? 409 : 400
            res.status(code);
            res.send(err);
        });

});




module.exports = router;