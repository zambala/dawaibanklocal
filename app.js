var express = require('express');
var app = express();
var server = require('http').createServer(app);
var bodyParser = require('body-parser');
var router = express.Router();

//middleware
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//app.use(multer({ dest: './uploads/'}));
app.use(function (req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  // res.header("Content-Type", "application/json");
  res.header("Accept-Encoding", "gzip");

  next();
});


router.use(function (req, res, next) {
  //authentication code goes here
  next();
});

app.use('/api/user', require('./controller/user_controller'));
app.use('/api/collectioncenter', require('./controller/collectioncenter_controller'));
app.use('/api/medicine', require('./controller/medicine_controller'));
app.use('/api/donar', require('./controller/donar_controller'));
app.use('/api/ticket', require('./controller/ticket_controller'));
app.use('/api/sendmail', require('./controller/email_controller'));
app.use('/api/delivermedicine', require('./controller/medicinedeliver_controller'));
app.use('/api/configlist', require('./controller/config_controller'));
app.use('/api/dropbox', require('./controller/dropbox_controller'));
app.use('/api/patient', require('./controller/patient_controller'));
app.use('/api/report', require('./controller/report_controller'));
app.use('/api/sms', require('./controller/sms_controller'));

//app.use('/api/shubhankartest', require('./controller/testshubhankar_controller'));
//app.use('/api/testsap', require('./controller/test_sapcontroller'));
//app.use('/api/test', require('./controller/test_controller'));
//app.use('/api/testsocket', require('./controller/test_socket'));

app.use((req, res, next) => {
  // console.log('********** ERRROR ******');
  const error = new Error('Not found');
  error.status = 404;
  next(error);
})

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
})


var PORT = 3000;
server.listen(PORT);
console.log('Server listenig on port ' + PORT);